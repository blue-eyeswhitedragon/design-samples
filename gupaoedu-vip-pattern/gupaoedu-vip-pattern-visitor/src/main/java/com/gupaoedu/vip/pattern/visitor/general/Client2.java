package com.gupaoedu.vip.pattern.visitor.general;

import java.util.ArrayList;
import java.util.List;

class Client2 {
    public static void main(String[] args) {
        ObjectStructure collection = new ObjectStructure();
        System.out.println("ConcreteVisitorA handle elements:");
        IVisitor visitorA = new ConcreteVisitorA();
        collection.accept(visitorA);
        System.out.println("------------------------------------");
        System.out.println("ConcreteVisitorB handle elements:");
        IVisitor visitorB = new ConcreteVisitorB();
        collection.accept(visitorB);

    }

    // 抽象元素
    interface IElement {
        void accept(IVisitor visitor);

        String operation();
    }

    // 具体元素A
    static class ConcreteElementA implements IElement {

        public void accept(IVisitor visitor) {
            visitor.visit(this);
        }

        public String operation() {
            return this.getClass().getSimpleName();
        }

    }

    // 具体元素B
    static class ConcreteElementB implements IElement {

        public void accept(IVisitor visitor) {
            visitor.visit(this);
        }

        public String operation() {
            return this.getClass().getSimpleName();
        }

    }

    // 具体元素B
    static class ConcreteElementC implements IElement {

        public void accept(IVisitor visitor) {
            visitor.visit(this);
        }

        public String operation() {
            return this.getClass().getSimpleName();
        }

    }

    // 抽象访问者
    interface IVisitor {
        void visit(IElement element);
    }

    // 具体访问者A
    static class ConcreteVisitorA implements IVisitor {

        public void visit(IElement element) {
            String result = element.operation();
            System.out.println("result from " + element.getClass().getSimpleName() + ": " + result);
        }

    }

    // 具体访问者B
    static class ConcreteVisitorB implements IVisitor {

        public void visit(IElement element) {
            String result = element.operation();
            System.out.println("result from " + element.getClass().getSimpleName() + ": " + result);
        }

    }

    // 结构对象
    static class ObjectStructure {
        private List<IElement> list = new ArrayList<IElement>();

        {
            this.list.add(new ConcreteElementA());
            this.list.add(new ConcreteElementB());
            this.list.add(new ConcreteElementC());
        }

        public void accept(IVisitor visitor) {
            for (IElement element : this.list) {
                element.accept(visitor);
            }
        }
    }
}